import { Component, OnInit } from '@angular/core';
import { AboutUsModel } from '../../cms/models/aboutUs.model';
import { WebsiteService } from '../websiteservice/website.service';

import { DanpheCareContact } from '../../cms/models/danphecare.cms.model';
import { NotificationService } from '../services/notification.service';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html'
})
export class DanpheFooterComponent implements OnInit {

  public aboutUs: AboutUsModel = new AboutUsModel();
  public contact: DanpheCareContact = new DanpheCareContact();

  constructor(public websiteService: WebsiteService, private notifyService: NotificationService) {
    this.GetAboutUs();
    this.GetContact();
  }
  ngOnInit() {
    this.GetAboutUs();
    this.GetContact();
  }
  GetAboutUs() {
    this.websiteService.GetAboutUs().subscribe(res => {
      if (res && res.length > 0) {
        this.aboutUs = Object.assign(this.aboutUs, res[0]);

      }
    },
      res => {
        //this.notifyService.showError("Error", "Internal Error")
      });
  }
  GetContact() {
    this.websiteService.GetContact().subscribe(res => {
      if (res && res.length > 0) {
        this.contact = Object.assign(this.contact, res[0]);

      }
    },
      res => {
        //this.notifyService.showError("Error", "Internal Error")
      });
  }

}
